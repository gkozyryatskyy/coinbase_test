package com.coinbase.config;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Map;
import java.util.Properties;

public enum Config {

    PATH_TO_CONFIG("COINBASE_CONFIG", "coinbase.properties"),
    ELASTICSEARCH_HOST("COINBASE_ELASTICSEARCH_HOST", "localhost"),
    ELASTICSEARCH_PORT("COINBASE_ELASTICSEARCH_PORT", "9200"),
    WEB_HOST("COINBASE_WEB_HOST", "0.0.0.0"),
    WEB_PORT("COINBASE_WEB_PORT", "8080"),
    WEB_PREFIX("COINBASE_WEB_PREFIX", "/v1/api"),
    INDEX("COINBASE_INDEX", "history"),
    TICKER_CHANNEL_ADDRESS("COINBASE_TICKER_CHANNEL_ADDRESS", "coinbase.ticker"),
    PERSIST_DATA_PERIOD("COINBASE_PERSIST_DATA_PERIOD", "5000");

    private String propertyName;
    private String defaultValue;

    Config(String propertyName, String defaultValue) {
        this.propertyName = propertyName;
        this.defaultValue = defaultValue;
    }

    // Configuration priority list
    // 1 System.getProperties() (-D parameters)
    // 2 System.getenv() (system environment variables)
    // 3 weavo.properties file

    static {
        loadIfNotExists(System.getenv());
        loadConfig(Config.PATH_TO_CONFIG.getString());
    }

    public static void loadConfig(String path) {
        Properties retval = loadFromClasspath(path);
        if (retval == null) {
            retval = loadFromAbsolute(path);
        }
        if (retval != null) {
            loadIfNotExists(retval);
        }
    }

    private static Properties loadFromClasspath(String path) {
        try (InputStream in = Config.class.getClassLoader().getResourceAsStream(path)) {
            if (in != null) {
                Properties props = new Properties();
                props.load(in);
                return props;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    private static Properties loadFromAbsolute(String path) {
        try (InputStream in = new FileInputStream(path)) {
            Properties props = new Properties();
            props.load(in);
            return props;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    private static void loadIfNotExists(Map<?, ?> configs) {
        Properties systemProps = System.getProperties();
        for (Map.Entry<?, ?> entry : configs.entrySet()) {
            systemProps.putIfAbsent(entry.getKey(), entry.getValue());
        }
    }

    public String getString() {
        return ConfigUtils.getString(propertyName, defaultValue);
    }

    public boolean getBoolean() {
        return ConfigUtils.getBoolean(propertyName, defaultValue);
    }

    public int getInt() {
        return ConfigUtils.getInt(propertyName, defaultValue);
    }

    public long getLong() {
        return ConfigUtils.getLong(propertyName, defaultValue);
    }

    public double getDouble() {
        return ConfigUtils.getDouble(propertyName, defaultValue);
    }

    public String[] getArray() {
        return ConfigUtils.getArray(propertyName, defaultValue);
    }

    }
