package com.coinbase.client.coinbase;

import java.util.List;
import java.util.function.Consumer;

import io.vertx.core.Future;
import io.vertx.core.Promise;
import io.vertx.core.Vertx;
import io.vertx.core.http.HttpClient;
import io.vertx.core.http.HttpClientOptions;
import io.vertx.core.http.WebSocket;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class CoinbaseWebsocket {

    private HttpClient client;

    public CoinbaseWebsocket(Vertx vertx) {
        this.client = vertx.createHttpClient(
                new HttpClientOptions().setDefaultHost("ws-feed.pro.coinbase.com").setSsl(true)
                        .setDefaultPort(443).setMaxWebSocketFrameSize(1048576));
    }

    public Future<WebSocket> subscribe(List<String> pairs, List<String> channels, Consumer<JsonObject> message) {
        Promise<WebSocket> retval = Promise.promise();
        client.webSocket("", e -> {
            if (e.succeeded()) {
                WebSocket websocket = e.result();
                websocket.handler(b -> message.accept(b.toJsonObject()));
                websocket.writeTextMessage(subscribeMessage(pairs, channels).toString());
                retval.complete(websocket);
            } else {
                retval.fail(e.cause());
            }
        });
        return retval.future();
    }

    public static JsonObject subscribeMessage(List<String> pairs, List<String> channels) {
        JsonObject subscribe = new JsonObject();
        subscribe.put("type", "subscribe");
        JsonArray products = new JsonArray(pairs);
        subscribe.put("product_ids", products);
        JsonArray channelsJson = new JsonArray(channels);
        subscribe.put("channels", channelsJson);
        return subscribe;
    }
}
