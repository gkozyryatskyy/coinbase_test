package com.coinbase;

import java.util.List;
import java.util.Objects;

import org.slf4j.LoggerFactory;

import com.coinbase.core.CoinbaseMicroservice;
import com.coinbase.core.WebApi;

import ch.qos.logback.classic.LoggerContext;
import io.vertx.core.DeploymentOptions;
import io.vertx.core.Future;
import io.vertx.core.Promise;
import io.vertx.core.Vertx;
import io.vertx.core.impl.CompositeFutureImpl;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class Main {

    private static Vertx vertx = Vertx.vertx();

    public static void main(String[] args) {
        // ShutdownHook
        Runtime.getRuntime().addShutdownHook(new Thread(Main::shutdown));
        // deploy
        JsonObject config = new JsonObject();
        config.put(CoinbaseMicroservice.CONFIG_PAIR_KEY, new JsonArray(List.of("BTC-USD", "ETH-USD")));
        config.put(CoinbaseMicroservice.CONFIG_CHANNELS_KEY, new JsonArray(List.of("ticker")));
        Future<String> coinbase = deploy(vertx, CoinbaseMicroservice.class, config);
        Future<String> api = deploy(vertx, WebApi.class, null);
        // start
        CompositeFutureImpl.join(api, coinbase).<Void>mapEmpty().setHandler(e -> {
            // post start
            log.info("................... Vertx started ...................");
        });
    }

    public static void shutdown() {
        vertx.close();
        log.info("................... Vertx shutdown ...................");
        ((LoggerContext) LoggerFactory.getILoggerFactory()).stop();
    }

    public static Future<String> deploy(Vertx vertx, Class<?> ms, JsonObject config) {
        Promise<String> retval = Promise.promise();
        String name = ms.getName();
        DeploymentOptions options = new DeploymentOptions();
        if (Objects.nonNull(config)) {
            options.setConfig(config);
        }
        vertx.deployVerticle(name, options, res -> {
            if (res.succeeded()) {
                String id = res.result();
                log.info("{}[{}] deployed. Deployment id is: {}", name, 1, id);
                retval.complete(id);
            } else {
                log.error("{} deployment exception. ", name, res.cause());
                retval.fail(res.cause());
            }
        });
        return retval.future();
    }

}
