package com.coinbase.config;

public class ConfigUtils {

    private static final String ARRAY_DELIMITER = ";";

    public static String getString(String key, String defaultValue) {
        String value = System.getProperty(key);
        return value == null ? defaultValue : value;
    }

    public static boolean getBoolean(String key, String defaultValue) {
        String value = System.getProperty(key);
        return value == null ? (defaultValue != null && Boolean.parseBoolean(defaultValue))
                : Boolean.parseBoolean(value);
    }

    public static int getInt(String key, String defaultValue) {
        String value = System.getProperty(key);
        return value == null ? (defaultValue != null ? Integer.parseInt(defaultValue) : 0) : Integer.parseInt(value);
    }

    public static long getLong(String key, String defaultValue) {
        String value = System.getProperty(key);
        return value == null ? (defaultValue != null ? Long.parseLong(defaultValue) : 0) : Long.parseLong(value);
    }

    public static double getDouble(String key, String defaultValue) {
        String value = System.getProperty(key);
        return value == null ? (defaultValue != null ? Double.parseDouble(defaultValue) : 0.0)
                : Double.parseDouble(value);
    }

    public static String[] getArray(String key, String defaultValue) {
        String value = System.getProperty(key);
        return value == null ? (defaultValue != null ? defaultValue.split(ARRAY_DELIMITER) : new String[0])
                : value.split(ARRAY_DELIMITER);
    }

}
