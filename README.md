# Build project
cd /path/to/project

mvn clean install
# Run infrastructure
cd {/path/to/project}/docker

docker-compose up -d
# Run project (+rebuild)
cd {/path/to/project}/docker

docker-compose -f project.yml up -d --build
# Api doc
http://localhost:8080/api-docs/#/
# WebSocket page
http://localhost:8080/api-docs/ws.html
# Data view
http://localhost:5601/app/kibana#/discover