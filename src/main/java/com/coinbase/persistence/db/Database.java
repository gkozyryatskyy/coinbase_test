package com.coinbase.persistence.db;

import org.apache.http.HttpHost;
import org.elasticsearch.client.RestClient;
import org.elasticsearch.client.RestHighLevelClient;

import com.coinbase.config.Config;

import lombok.Getter;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Getter
public class Database {

    private final RestHighLevelClient client;

    private Database(RestHighLevelClient client) {
        this.client = client;
    }

    public static Database initClient(String hostname, int port) {
        try {
            RestHighLevelClient client = new RestHighLevelClient(
                    RestClient.builder(new HttpHost(hostname, port, HttpHost.DEFAULT_SCHEME_NAME)));
            log.info("[elasticseach] client started successfylly on " + hostname + ":" + port);

            return new Database(client);
        } catch (Throwable e) {
            throw new IllegalStateException("[elasticseach] instantiation exception. ", e);
        }
    }

    public static Database getInstance() {
        return InstanceHolder.instance;
    }

    public static RestHighLevelClient getClient() {
        return getInstance().client;
    }

    private static class InstanceHolder {
        static final Database instance = initClient(Config.ELASTICSEARCH_HOST.getString(),
                Config.ELASTICSEARCH_PORT.getInt());
    }
}
