package com.coinbase.client;

import java.util.List;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;

import com.coinbase.client.coinbase.CoinbaseWebsocket;

import io.vertx.core.Vertx;
import io.vertx.ext.unit.Async;
import io.vertx.ext.unit.TestContext;
import io.vertx.ext.unit.junit.VertxUnitRunner;
import lombok.extern.slf4j.Slf4j;

@Ignore
@Slf4j
@RunWith(VertxUnitRunner.class)
public class CoinbaseWebsocketTest {

    public static Vertx vertx;

    @BeforeClass
    public static void start() {
        vertx = Vertx.vertx();
    }

    @AfterClass
    public static void stop(TestContext context) {
        Async clusterCompletion = context.async();
        vertx.close(e -> {
            if (e.succeeded()) {
                clusterCompletion.complete();
            } else {
                context.fail(e.cause());
            }
        });
    }

    @Test
    public void wsTest(TestContext context) {
        Async completion = context.async();
        new CoinbaseWebsocket(vertx).subscribe(List.of("BTC-USD", "ETH-USD"), List.of("ticker"), message -> {
            System.out.println(message);
            //{"type":"ticker","sequence":12383356915,"product_id":"BTC-USD","price":"10231.02","open_24h":"9929.05000000","volume_24h":"12011.09755452","low_24h":"9928.08000000","high_24h":"10315.00000000","volume_30d":"309356.81722907","best_bid":"10228.43","best_ask":"10231.02","side":"buy","time":"2020-02-19T17:44:41.347585Z","trade_id":83235889,"last_size":"0.01308137"}
        });
    }
}
