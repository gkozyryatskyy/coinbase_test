package com.coinbase.core;

import static java.util.Objects.nonNull;

import com.coinbase.client.HttpException;
import com.coinbase.config.Config;
import com.coinbase.core.http.WsService;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.Promise;
import io.vertx.core.http.HttpHeaders;
import io.vertx.core.http.HttpServer;
import io.vertx.core.http.HttpServerOptions;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.RoutingContext;
import io.vertx.ext.web.handler.BodyHandler;
import io.vertx.ext.web.handler.StaticHandler;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class WebApi extends AbstractVerticle {

    private static final String WEB_PREFIX = Config.WEB_PREFIX.getString();
    private static final int WEB_PORT = Config.WEB_PORT.getInt();
    private static final String WEB_HOST = Config.WEB_HOST.getString();

    protected HttpServer server;

    @Override
    public void start(Promise<Void> start) {
        this.server = vertx.createHttpServer(new HttpServerOptions().setHost(WEB_HOST).setPort(WEB_PORT));
        Router router = Router.router(vertx);
        // errors
        router.route(WEB_PREFIX + "/*").handler(BodyHandler.create()).failureHandler(this::handleFailure);
        // all options
        router.options().handler(c -> c.response().end());
        // logging
        router.route(WEB_PREFIX + "/*").handler(c -> {
            c.response().putHeader(HttpHeaders.CONTENT_TYPE, "application/json; charset=utf-8");
            // request logging
            log.debug("[{}:{}] body [{}] ", c.request().method(), c.request().uri(), c.getBodyAsString());
            c.next();
        });
        // api doc
        router.route("/api-docs/*").handler(StaticHandler.create());
        // services
        WsService wsService = new WsService();
        router.get(WEB_PREFIX + "/ws").handler(wsService::download);
        // websocket
        server.webSocketHandler(ws ->{
            vertx.eventBus().<JsonObject>consumer(Config.TICKER_CHANNEL_ADDRESS.getString(), message -> {
                if (!ws.isClosed()) {
                    ws.writeBinaryMessage(message.body().toBuffer());
                }
            });
        });
        // start server
        server.requestHandler(router).listen();
        start.complete();
    }

    public void handleFailure(RoutingContext context) {
        Throwable cause = context.failure();
        log.error("[{}:{}] Handler exception. ", context.request().method(), context.request().uri(), cause);
        if (cause == null) {
            respondError(context, context.statusCode(), null);
        } else if (cause instanceof HttpException) {
            HttpException ex = (HttpException) cause;
            respondError(context, ex.getStatusCode(), ex);
        } else {
            respondError(context, 500, cause);
        }
    }

    public void respondError(RoutingContext context, int status, Throwable cause) {
        if (nonNull(cause) && nonNull(cause.getMessage())) {
            context.response().setStatusCode(status).end(new JsonObject().put("message", cause.getMessage()).encode());
        } else {
            context.response().setStatusCode(status).end();
        }
    }

}
