FROM openjdk:13-buster

RUN mkdir -p /usr/src/app
WORKDIR /usr/src/app
ADD ./target /usr/src/app

MAINTAINER Gleb Kozyryatskyy

#using upper case name with out dots because of CMD command can`t use them
ENV COINBASE_VERSION=0.0.1
ENV COINBASE_ELASTICSEARCH_HOST=coinbase-es

EXPOSE 8080

#exec helps us to receive a shutdown signal. Use exec or CMD ["cmd1", "cmd2"] syntax, but it is not support environment
CMD exec java -jar $JAVA_OPTS /usr/src/app/coinbase-$COINBASE_VERSION.jar
