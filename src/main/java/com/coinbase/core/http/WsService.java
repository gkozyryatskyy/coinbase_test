package com.coinbase.core.http;

import javax.ws.rs.Path;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.vertx.core.http.HttpHeaders;
import io.vertx.ext.web.RoutingContext;

@Path("/ws")
@Api(value = "WebSocketService")
public class WsService {

    @Path("/")
    @ApiOperation(httpMethod = "GET", value = "redirect", notes = "Redirect to ws page.")
    @ApiResponses({ @ApiResponse(code = 200, message = "OK") })
    public void download(RoutingContext context) {
        context.response()
                .putHeader(HttpHeaders.LOCATION, "/api-docs/ws.html")
                .setStatusCode(301)
                .end();
    }

}
