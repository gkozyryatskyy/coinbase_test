package com.coinbase.core;

import java.util.ArrayList;
import java.util.List;

import org.elasticsearch.action.ActionListener;
import org.elasticsearch.action.bulk.BulkRequest;
import org.elasticsearch.action.bulk.BulkResponse;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.common.xcontent.XContentType;

import com.coinbase.client.coinbase.CoinbaseWebsocket;
import com.coinbase.config.Config;
import com.coinbase.persistence.db.Database;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.Promise;
import io.vertx.core.http.WebSocket;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class CoinbaseMicroservice extends AbstractVerticle {

    public static final String CONFIG_PAIR_KEY = "pairs";
    public static final String CONFIG_CHANNELS_KEY = "channels";

    public static final String INDEX = Config.INDEX.getString();
    public static final String CHANNEL = Config.TICKER_CHANNEL_ADDRESS.getString();

    protected WebSocket websocket;
    protected BulkRequest bulk = new BulkRequest(INDEX);

    public List<String> getArrayConfig(String key) {
        JsonArray arr = config().getJsonArray(key);
        List<String> retval = new ArrayList<>();
        for (int i = 0; i < arr.size(); i++) {
            retval.add(arr.getString(i));
        }
        return retval;
    }

    @Override
    public void start(Promise<Void> start) {
        vertx.setPeriodic(Config.PERSIST_DATA_PERIOD.getLong(), t -> {
            if (!bulk.requests().isEmpty()) {
                Database.getClient().bulkAsync(bulk, RequestOptions.DEFAULT, new ActionListener<>() {
                    @Override
                    public void onResponse(BulkResponse bulkItemResponses) {
                        if (bulkItemResponses.hasFailures()) {
                            log.warn(bulkItemResponses.buildFailureMessage());
                        } else {
                            log.trace("Successfully persisted {}", bulkItemResponses.getItems().length);
                        }
                    }

                    @Override
                    public void onFailure(Exception e) {
                        log.error("Error. ", e);
                    }
                });
                bulk = new BulkRequest(INDEX);
            }
        });
        new CoinbaseWebsocket(vertx).subscribe(getArrayConfig(CONFIG_PAIR_KEY), getArrayConfig(CONFIG_CHANNELS_KEY),
                this::processMessage).setHandler(e -> {
            if (e.succeeded()) {
                this.websocket = e.result();
                start.complete();
            } else {
                start.fail(e.cause());
            }
        });
    }

    public void processMessage(JsonObject message) {
        if ("ticker".equals(message.getString("type"))) {
            bulk.add(new IndexRequest().source(docMessage(message).encode(), XContentType.JSON));
            vertx.eventBus().publish(CHANNEL, channelMessage(message));
        } else {
            log.warn("Unsupported message type: {}", message);
        }
    }

    public JsonObject docMessage(JsonObject message) {
        // cast times because all is string
        JsonObject retval = new JsonObject();
        retval.put("product_id", message.getString("product_id"));
        retval.put("price", Double.parseDouble(message.getString("price")));
        retval.put("open_24h", Double.parseDouble(message.getString("open_24h")));
        retval.put("volume_24h", Double.parseDouble(message.getString("volume_24h")));
        retval.put("low_24h", Double.parseDouble(message.getString("low_24h")));
        retval.put("high_24h", Double.parseDouble(message.getString("high_24h")));
        retval.put("volume_30d", Double.parseDouble(message.getString("volume_30d")));
        retval.put("best_bid", Double.parseDouble(message.getString("best_bid")));
        retval.put("best_ask", Double.parseDouble(message.getString("best_ask")));
        retval.put("side", message.getString("side"));
        retval.put("time", message.getString("time"));
        retval.put("last_size", Double.parseDouble(message.getString("last_size")));
        return retval;
    }

    public JsonObject channelMessage(JsonObject message) {
        // cast times because all is string
        JsonObject retval = new JsonObject();
        retval.put("product_id", message.getString("product_id"));
        retval.put("price", Double.parseDouble(message.getString("price")));
        retval.put("best_bid", Double.parseDouble(message.getString("best_bid")));
        retval.put("best_ask", Double.parseDouble(message.getString("best_ask")));
        return retval;
    }
}
